# SPDX-FileCopyrightText: 2012 Mattia Basaglia <dev@dragon.best>
# SPDX-License-Identifier: LGPL-3.0-or-later

QT       += core gui

OBJECTS_DIR = ../out/obj
MOC_DIR = ../out/generated
UI_DIR = ../out/generated
RCC_DIR = ../out/generated

greaterThan(QT_MAJOR_VERSION, 4): QT += widgets

TARGET = history_lineedit
TEMPLATE = app


SOURCES += main.cpp 


include(../history_lineedit.pri)


