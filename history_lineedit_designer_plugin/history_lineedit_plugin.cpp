/*
 * SPDX-FileCopyrightText: 2012 Mattia Basaglia <dev@dragon.best>
 *
 * SPDX-License-Identifier: LGPL-3.0-or-later
 */

#include "history_lineedit_plugin.hpp"
#include "history_line_edit.hpp"

History_LineEdit_Plugin::History_LineEdit_Plugin(QObject *parent) :
    QObject(parent), initialized(false)
{
}

void History_LineEdit_Plugin::initialize(QDesignerFormEditorInterface *)
{
    initialized = true;
}

bool History_LineEdit_Plugin::isInitialized() const
{
    return initialized;
}

QWidget *History_LineEdit_Plugin::createWidget(QWidget *parent)
{
    return new HistoryLineEdit(parent);
}

QString History_LineEdit_Plugin::name() const
{
    return "HistoryLineEdit";
}

QString History_LineEdit_Plugin::group() const
{
    return "Input Widgets";
}

QIcon History_LineEdit_Plugin::icon() const
{
    return QIcon::fromTheme("edit-rename");
}

QString History_LineEdit_Plugin::toolTip() const
{
    return "Line Edit with history";
}

QString History_LineEdit_Plugin::whatsThis() const
{
    return "A QLineEdit that remembers and allows to navigate the history of lines  entered in the widget";
}

bool History_LineEdit_Plugin::isContainer() const
{
    return false;
}

QString History_LineEdit_Plugin::domXml() const
{
    return "<ui language=\"c++\">\n"
           " <widget class=\"HistoryLineEdit\" name=\"history_line_edit\">\n"
           " </widget>\n"
           "</ui>\n";
}

QString History_LineEdit_Plugin::includeFile() const
{
    return "history_line_edit.hpp";
}


