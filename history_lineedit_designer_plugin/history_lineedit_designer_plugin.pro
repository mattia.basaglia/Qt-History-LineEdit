# SPDX-FileCopyrightText: 2012 Mattia Basaglia <dev@dragon.best>
# SPDX-License-Identifier: LGPL-3.0-or-later

CONFIG += plugin
QT += designer widgets
TARGET = history_lineedit
TEMPLATE = lib
OBJECTS_DIR = ../out/obj
MOC_DIR = ../out/generated
UI_DIR = ../out/generated
RCC_DIR = ../out/generated

SOURCES += \
    history_lineedit_plugin.cpp \
    history_lineedit_plugin_collection.cpp

HEADERS += \
    history_lineedit_plugin.hpp \
    history_lineedit_plugin_collection.hpp

include(../history_lineedit.pri)

build_all:!build_pass {
 CONFIG -= build_all
 CONFIG += release
}

# install
target.path += $$[QT_INSTALL_PLUGINS]/designer

unix{
    LIB_TARGET = lib$${TARGET}.so
}
win32 {
    LIB_TARGET = $${TARGET}.dll
}
creator.files =$$LIB_TARGET
creator.path = $$[QT_INSTALL_PREFIX]/../../Tools/QtCreator/bin/designer
INSTALLS += target creator

